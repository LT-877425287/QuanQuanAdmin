import Axios from 'axios'
import Config from './config'
import {
    Notification,
    Loading
} from 'element-ui';


//创建请求
const service = Axios.create({
    baseURL: Config.apiUrl + '/' + Config.apiPrefix,
    headers: {
        'Accept': '*/*'
    },
    timeout: Config.timeout
})

service.defaults.retry = Config.requestRetry;    //全局请求次数
service.defaults.retryDelay = Config.requestRetryDelay;   //请求间隙

//请求
//增加请求拦截器
service.interceptors.request.use(      
    config => {
        if (!config.closeLoading) {         //closeLoading  关闭加载
            window.loadingInstance = Loading.service({    //调用loading
                fullscreen: true
            });
        }
        return config
    },
    error => {
        Promise.reject(error)
    }
);

//响应
service.interceptors.response.use(
    response => {

        const res = response;
        if (!response.config.closeLoading) {
            window.loadingInstance.close();
        }

        if (res.status !== 200) {//请求失败
            Notification({
                title: '数据返回出错',
                message: "请稍后重试",
                type: 'warning'
            });
            return Promise.reject('error')
        }


        if ((response.config).hasOwnProperty('closeInterceptors') && response.config.closeInterceptors) {  //关闭拦截的判断 前面防止你写closeInterceptors属性名
            return res.data    //{"code":0,"msg":"异常信息","data":"a:1"}
        }

        if (res.data.code != 0) {//所有请求成功返回的状态码code为0  如果不等于0请求失败，提示错误信息
            Notification({
                title: res.data.msg,
                type: 'warning'
            });
            return Promise.reject('error');
        }

        //请求成功只返回data数据  前端只处理data数据，无需管理msg，code
        return res.data.data;

    },
    error => {
        console.log("error:" + error)
        if (error == 'Error: Request failed with status code 403') {
            Notification({
                title: '请求失败',
                message: "身份验证失效,请重新登录",
                type: 'warning'
            });
            window.loadingInstance.close();
            router.push("/");
        } else {
            setTimeout(_ => {
                window.loadingInstance.close();
            }, 300)
            Notification({
                title: "请求未响应",
                message: "服务器可能出了点问题",
                type: 'warning'
            });
            return Promise.reject(error) //千万不能去掉，，，否则请求超时会进入到then方法，导致逻辑错误。
        }
    }
)

export default service
