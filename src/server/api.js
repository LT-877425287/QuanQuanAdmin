import request from './request'

//无参
export function NoParameter(params) {
    return request({
        url: '/NoParameter',
        method: 'post',
        data: params
    })
}

//带参
export function Parameter(params) {
    return request({
        url: '/Parameter',
        method: 'post',
        data: params
    })
}

//请求方式post
export function post(params) {
    return request({
        url: '/PG',
        method: 'post',
        data: params
    })
}
//请求方式post
export function get(params) {
    return request({
        url: '/PG',
        method: 'get',
        data: params
    })
}
