const devApiUrl = 'http://47.56.15.123:8025';//本地开发环境
const buildDevApiUrl = 'http://192.168.0.99:8025';//打包开发环境
const buildTestApiUrl = 'http://192.168.0.99:8025';//打包测试环境
const buildProApiUrl = 'http://47.56.15.123:8025';//打包正式环境


let useApiUrl;

switch (process.env.NODE_ENV) {
    case 'development':
        useApiUrl = devApiUrl;
        break;
    case 'buildDev':
        useApiUrl = buildDevApiUrl;
        break;
    case 'buildTest':
        useApiUrl = buildTestApiUrl;
        break;
    case 'production':
        useApiUrl = buildProApiUrl;
        break;
}

export default {
    nodeDevEnv: process.env.NODE_ENV == 'development',
    apiUrl: useApiUrl,
    apiPrefix: "",
    timeout: 5000,
    cookiesExpires: 7,
    requestRetry: 0,
    requestRetryDelay: 800,
}
