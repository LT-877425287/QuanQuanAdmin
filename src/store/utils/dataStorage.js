//对浏览器的Cookies和Session  可以参考https://www.cnblogs.com/houzheng/p/9067110.html
//注意Cookies和Session这里面的数据虽然全局可以通过以下方式可以操作，
// 但是界面不刷新的话这些便更的数据无法在界面上做到响应式，也就是store的state中的头文件描述的第3句
import Storage from 'good-storage'

//Cookies
export function CookiesPut(key, value) {
    return Storage.set(key, value)
}
export function CookiesGet(key) {
    return Storage.get(key)
}
export function CookiesClear() {
    return Storage.clear()
}
export function CookiesRemove(key) {
    return Storage.remove(key)
}


//Session
export function SessionPut(key, value) {
    return Storage.session.set(key, value)
}
export function SessionGet(key) {
    return Storage.session.get(key)
}
export function SessionClear() {
    return Storage.session.clear()
}
export function SessionRemove(key) {
    return Storage.session.remove(key)
}
