//异步全局请求就放这里存着
import {USER_INFO} from "./mutation-types";

export default {
    //context 上下文 可以理解为context就是state
    //payload 入参data
    asynUpUserInfo(context, payload) {
        //创建回调对象
        return new Promise((resolve, reject) => {

            setTimeout(() => {//模拟异步请求
                console.log(payload);
                //模拟成功回调resolve  失败回调reject  的数据
                if (Math.ceil(Math.random() * 10) > 5) {
                    let aUpUserInfo = {
                        id: "0004",
                        name: 'T',
                        age: '24'
                    }
                    //异步操作成功，提交数据
                    context.commit(USER_INFO,aUpUserInfo);
                    resolve({msg: '成功回调'});
                } else {
                    reject({msg: '失败回调'});
                }
            }, 2000)
        })
    }
}
