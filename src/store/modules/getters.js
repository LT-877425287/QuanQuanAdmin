//相当于钩子函数中的计算属性
export default {
    getUserInfo_age(state) {
        //过滤掉players中age>18的玩家
        return state.players.filter(p=>p.age>18);
    },
}
